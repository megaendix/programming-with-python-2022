import re


def read_input():
    monkeys = []
    with open("day11.txt", "rt") as monkey_logic:
        current_items = []
        current_operation = ""
        current_clause = []
        for line in monkey_logic:
            if "Monkey" in line:
                continue
            if "Starting" in line:
                number_sequence = line.strip().split(":")[1]
                current_items = list(map(lambda x: int(x), number_sequence.split(",")))

            if "Operation" in line:
                current_operation = line.split(":")[1].split("=")[1].strip()

            if "Test" in line:
                current_clause = [int(re.search(r"\d+", line).group())]

            if "true" in line:
                current_clause.append(int(re.search(r"\d+", line).group()))

            if "false" in line:
                current_clause.append(int(re.search(r"\d+", line).group()))
                monkeys.append([current_items, current_operation, current_clause])
    return monkeys


def follow_the_apes():
    monkeys = read_input()
    monkey_inspection_times = list(map(lambda x: 0, range(0, len(monkeys))))

    for n in range(0, 20):
        for index, monkey in enumerate(monkeys):
            monkey_operation = monkey[1]
            monkey_clause = monkey[2]
            for item in monkey[0]:
                monkey_inspection_times[index] += 1
                current_worry = calculate_operation(monkey_operation, item)
                current_worry = current_worry // 3

                if current_worry % monkey_clause[0] == 0:
                    monkeys[monkey_clause[1]][0].append(current_worry)
                else:
                    monkeys[monkey_clause[2]][0].append(current_worry)
            monkey[0].clear()
    highest_value = max(monkey_inspection_times)
    monkey_inspection_times.remove(highest_value)
    return highest_value * max(monkey_inspection_times)


def calculate_operation(operation, current_worry):
    assert isinstance(operation, str)
    assert isinstance(current_worry, int)
    new_worry = current_worry
    operation = operation.split()
    match operation[1:3]:
        case ["+", i]:
            if i == "old":
                i = current_worry
            new_worry += int(i)
        case ["-", i]:
            if i == "old":
                i = current_worry
            new_worry -= int(i)
        case ["*", i]:
            if i == "old":
                i = current_worry
            new_worry *= int(i)
    return new_worry


def follow_the_apes_INFINITE():
    monkeys = read_input()
    monkey_inspection_times = list(map(lambda x: 0, range(0, len(monkeys))))
    the_mod_of_god_list = (list(map(lambda x: x[2][0], monkeys)))
    the_mod_of_god = the_mod_of_god_list[0]
    for value in the_mod_of_god_list[1:len(the_mod_of_god_list)]:
        the_mod_of_god *= value

    for n in range(0, 10000):
        for index, monkey in enumerate(monkeys):
            monkey_operation = monkey[1]
            monkey_clause = monkey[2]
            for item in monkey[0]:
                monkey_inspection_times[index] += 1
                current_worry = calculate_operation(monkey_operation, item)

                if current_worry % monkey_clause[0] == 0:
                    monkeys[monkey_clause[1]][0].append(current_worry % the_mod_of_god)
                else:
                    monkeys[monkey_clause[2]][0].append(current_worry % the_mod_of_god)
            monkey[0].clear()
    highest_value = max(monkey_inspection_times)
    monkey_inspection_times.remove(highest_value)
    return highest_value * max(monkey_inspection_times)


print(f"Answer for Part 1: {follow_the_apes()}")
# 66124 is the right answer

print(f"Answer for Part 2: {follow_the_apes_INFINITE()}")
# 19309892877


