def JanKenPon(f):
    translatorDict = dict()
    translatorDict["A"] = "Rock"
    translatorDict["X"] = "Rock"
    translatorDict["B"] = "Paper"
    translatorDict["Y"] = "Paper"
    translatorDict["C"] = "Scissors"
    translatorDict["Z"] = "Scissors"

    valueDict = dict(X=1, Y=2, Z=3)

    matchupDict = dict(X="C", Y="A", Z="B")

    myScore = 0
    with open(f, "rt") as games:
        for game in games:
            enemyMove, myMove = game.strip().split()
            myScore += valueDict[myMove]
            myScore += 3 if translatorDict[enemyMove] == translatorDict[myMove] else (6 if matchupDict[myMove] == enemyMove else 0)
    return myScore


def JanKenPon2(f):
    translatorDict = dict()
    translatorDict["A"] = "Rock"
    translatorDict["X"] = "Rock"
    translatorDict["B"] = "Paper"
    translatorDict["Y"] = "Paper"
    translatorDict["C"] = "Scissors"
    translatorDict["Z"] = "Scissors"

    valueDict = dict(A=1, B=2, C=3)

    valueDict2 = dict(X=1, Y=2, Z=3)

    matchupDict = dict(X="C", Y="A", Z="B")

    loseDict = dict(A="Z", B="X", C="Y")

    winDict = dict(A="Y", B="Z", C="X")

    toDoDict = dict(X=0, Y=3, Z=6)

    myScore = 0
    with open(f, "rt") as games:
        for game in games:
            enemyMove, myMove = game.strip().split()
            moveScore = toDoDict[myMove]
            myScore += moveScore
            match moveScore:
                case 0:
                    myScore += valueDict2[loseDict[enemyMove]]
                case 3:
                    myScore += valueDict[enemyMove]
                case 6:
                    myScore += valueDict2[winDict[enemyMove]]
    return myScore


part1 = JanKenPon("day02.txt")
part2 = JanKenPon2("day02.txt")
print(f"That's for Part 1:\nIf everything would go according to the strategy guide the score would be: {part1}\n")
print(f"That's for Part 2:\nIf everything would go according to the (correct) strategy guide the score would be: {part2}")

