def findDuplcates(f):
    sumOfPrios = 0
    with open(f, "rt") as bags:
        for bag in bags:
            l = len(bag)
            s1 = set(bag[0:l // 2])
            s2 = set(bag[l // 2:l])
            duplicate = s1 & s2
            element = str(duplicate)[2]
            sumOfPrios += prio_of_item(element)
    return sumOfPrios


def prio_of_item(item):
    if item.isupper():
        return ord(item) - 38
    return ord(item) - 96


def find_badges_and_sum_up(f):
    sum_of_prios = 0
    with open(f, "rt") as all_bags:
        bag3 = []
        for bag in all_bags:
            bag3.append(bag)
            if len(bag3) == 3:
                badge = find_same_item_in_three(bag3)
                sum_of_prios += prio_of_item(badge)
                bag3.clear()
    return sum_of_prios



def find_same_item_in_three(bags):
    set_list = []

    for bag in bags:
        bag = bag.strip()
        set_list.append(set(bag))
    duplicate = set_list[0] & set_list[1] & set_list[2]

    for item in duplicate:
        found = True
        for bag in bags:
            if item not in bag:
                found = False
                break
        if found:
            return item

prios = findDuplcates("day03.txt")
print(f"That's for Part 1:\nThe sum of the priorities is: {prios}\n")

badgePrio = find_badges_and_sum_up("day03.txt")
print(f"That's for Part 2:\nThe sum of the new priorities is: {badgePrio}\n")
