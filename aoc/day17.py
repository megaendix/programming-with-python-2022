import itertools
import copy


def read_input():
    jet_list = []
    with open("day17.txt", "rt") as jets:
        for line in jets:
            line = line.strip()
            for jet in line:
                jet_list.append(jet)
    return jet_list


class Block:
    def __init__(self, p=[]):
        self.squares = p

    def __repr__(self):
        points = str(self.squares)
        return f"Block: {points}"

    def create_horizontal_beam(self):
        for x in range(0, 4):
            self.squares.append([x + 2, 0])

    def create_vertical_beam(self):
        for y in range(0, 4):
            self.squares.append([0 + 2, y])

    def create_cross(self):
        for y in range(0, 3):
            self.squares.append([1 + 2, y])
        self.squares.append([0 + 2, 1])
        self.squares.append([2 + 2, 1])

    def create_L(self):
        for y in range(0, 3):
            self.squares.append([2 + 2, y])

        self.squares.append([0 + 2, 0])
        self.squares.append([1 + 2, 0])

    def create_square(self):
        for x in range(0, 2):
            for y in range(0, 2):
                self.squares.append([x + 2, y])

    def copy_block(self):
        return Block(copy.deepcopy(self.squares))

    def get_all_x_coordinates(self):
        xes = []
        for point in self.squares:
            xes.append(point[0])
        return xes

    def get_squares(self):
        return self.squares

    def move_block_to_start(self, height):
        for index in range(0, len(self.squares)):
            self.squares[index][1] += height + 3

    def fall_down_one(self):
        for index in range(0, len(self.squares)):
            self.squares[index][1] -= 1

    def move_block_right(self):
        for index in range(0, len(self.squares)):
            self.squares[index][0] += 1

    def move_block_left(self):
        for index in range(0, len(self.squares)):
            self.squares[index][0] -= 1

    def move_block_up(self):
        for index in range(0, len(self.squares)):
            self.squares[index][1] += 1

    def move_block(self, direction):
        match direction:
            case "down":
                self.fall_down_one()
            case "<":
                self.move_block_left()
            case ">":
                self.move_block_right()

    def reverse_move(self, direction):
        match direction:
            case "<":
                self.move_block_right()
            case ">":
                self.move_block_left()
            case "down":
                self.move_block_up()


class PlayingField:
    def __init__(self, x=7):
        self.width = x
        self.busy_spaces = set()
        for x_cord in range(0, x):
            self.busy_spaces.add((x_cord, -1))

    def add_block(self, block):
        for b in block.get_squares():
            self.busy_spaces.add(tuple(b))

    def get_height(self):
        list_of_y = []
        for square in self.busy_spaces:
            list_of_y.append(square[1])
        return max(list_of_y) + 1

    def check_if_block_is_hitting(self, block):
        my_xes = block.get_all_x_coordinates()
        # checks bounds
        if min(my_xes) < 0 or self.width <= max(my_xes):
            return True

        for point in block.get_squares():
            if tuple(point) in self.busy_spaces:
                return True
        return False

    def check_if_can_move(self, block, dir="down"):
        simulation_block = block.copy_block()
        simulation_block.move_block(dir)

        return not self.check_if_block_is_hitting(simulation_block)


def let_them_fall():
    jets = read_input()
    rocks = [x for x in range(0, 5)]
    jets_buffer = itertools.cycle(jets)
    rocks_buffer = itertools.cycle(rocks)
    cave = PlayingField()

    for count in range(0, 2022):
        rock = rocks_buffer.__next__()
        height = cave.get_height()
        current_block = Block([])
        match rock:
            case 0:
                current_block.create_horizontal_beam()
            case 1:
                current_block.create_cross()
            case 2:
                current_block.create_L()
            case 3:
                current_block.create_vertical_beam()
            case _:
                current_block.create_square()
        current_block.move_block_to_start(height)
        falling = True
        # print(f"Next move starts:\n{visual_output(cave, current_block)}")
        # input()
        while falling:
            move = jets_buffer.__next__()
            if not cave.check_if_can_move(current_block, move):
                if not cave.check_if_can_move(current_block, "down"):
                    cave.add_block(current_block)
                    # print(f"Rock now still:\n{visual_output(cave, current_block)}")
                    # input()
                    break
                current_block.move_block("down")
                # print(f"Rock moved one down:\n{visual_output(cave, current_block)}")
                # input()
                continue

            current_block.move_block(move)
            # print(f"Rock made Move:\n{visual_output(cave, current_block)}")
            # input()

            if not cave.check_if_can_move(current_block, "down"):
                cave.add_block(current_block)
                # print(f"Rock now still:\n{visual_output(cave, current_block)}")
                # input()
                break

            current_block.move_block("down")
            # print(f"Rock moved one down:\n{visual_output(cave, current_block)}")
            # input()
    # print(visual_output(cave, Block()))
    return cave.get_height()


def visual_output(cave, block):
    assert isinstance(cave, PlayingField)
    assert isinstance(block, Block)
    output = ""
    width = cave.width
    height = cave.get_height() + 6
    for y in range(height - 1, -2, -1):
        for x in range(-1, width + 1):
            if y == -1:
                output += "="
                continue
            if x == -1 or x == width:
                output += "|"
                continue
            current_position = (x, y)
            if current_position in cave.busy_spaces:
                output += "#"
            else:
                if list(current_position) in block.get_squares():
                    output += "@"
                else:
                    output += "."
        output += "\n"
    return output


print(f"That's for Part 1:\n End height is: {let_them_fall()}")
# 3161

