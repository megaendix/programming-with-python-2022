import re
import time


def read_input():
    sensors = []
    beacons = []
    with open("day15.txt", "rt") as positions:
        for line in positions:
            sensor, beacon = line.split(":")
            sensor = re.search(r"x=(-?\d*), y=(-?\d*)", sensor)
            beacon = re.search(r"x=(-?\d*), y=(-?\d*)", beacon)
            sensors.append((int(sensor.group(1)), int(sensor.group(2))))
            beacons.append((int(beacon.group(1)), int(beacon.group(2))))
    field = dict()
    for sensor in sensors:
        field[sensor] = "S"
    for beacon in beacons:
        field[beacon] = "B"
    return sensors, beacons, field


def draw_mainhatten_distance(field, sensor, beacon, minX, maxX, line):
    x_sensor = sensor[0]
    y_sensor = sensor[1]
    distance = calc_mainhatten_disctance(sensor, beacon)

    if line < y_sensor - distance or y_sensor + distance < line:
        return field, minX, maxX

    for x in range(x_sensor - distance, x_sensor + distance + 1):
        if calc_mainhatten_disctance(sensor, (x, line)) > distance:
            continue
        field[x, line] = "#"

    minX = minX if minX < x_sensor - distance else x_sensor - distance
    maxX = maxX if maxX > x_sensor + distance else x_sensor + distance

    return field, minX, maxX


def calc_mainhatten_disctance(sensor, beacon):
    return abs(sensor[0] - beacon[0]) + abs(sensor[1] - beacon[1])


def where_arent_the_beacons(line=2_000_000):
    # current_time = time.time()
    sensors, beacons, field = read_input()
    # print("Time after reading_input --- %s seconds ---" % (time.time() - current_time))
    # current_time = time.time()
    minX = 0
    maxX = 0
    for index in range(0, len(sensors)):
        field, minX, maxX = draw_mainhatten_distance(field, sensors[index], beacons[index], minX, maxX, line)

    # print("Time after creating field --- %s seconds ---" % (time.time() - current_time))
    # current_time = time.time()

    not_possible_positions = 0
    for x in range(minX, maxX + 1):
        if (x, line) in field:
            not_possible_positions += 1

    # print("Time after checking positions --- %s seconds ---" % (time.time() - current_time))
    # current_time = time.time()

    for beacon in beacons:
        if beacon[1] == line:
            not_possible_positions -= 1
            break

    # print("--- %s seconds ---" % (time.time() - current_time))
    return not_possible_positions


print(f"That's for Part 1:\nNot possible Beacon positions for y=2.000.000 are {where_arent_the_beacons()}")
# 6124805
# 16.150146722793579 sec


