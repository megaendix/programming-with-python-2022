
def read_input():
    field = dict()
    starting_position = None
    current_digit = ""
    instructions = []
    done = False
    with open("day22.text", "rt") as my_file:
        for y, line in enumerate(my_file):
            if line == "\n":
                done = True
                continue
            for x, character in enumerate(line):
                if not done:
                    if character == ".":
                        if starting_position is None:
                            starting_position = (x, y)
                        field[x, y] = True
                        continue
                    if character == "#":
                        field[x, y] = False
                        continue
                if character == "L" or character == "R":
                    if len(current_digit) != 0:
                        instructions.append(int(current_digit))
                        current_digit = ""
                    instructions.append(character)
                    continue
                current_digit += character
        instructions.append(int(current_digit))
    my_map = Map(field, starting_position)
    return my_map, instructions


class Map:
    def __init__(self, field, starting_pos):
        self.field = field
        self.width = 0
        self.height = 0
        for entry in field:
            if entry[1] > self.height:
                self.height = entry[1]
            if entry[0] > self.width:
                self.width = entry[0]
        self.width += 1
        self.height += 1
        self.position = starting_pos

    def move_right(self):
        if (new_pos := (self.position[0] + 1, self.position[1])) in self.field:
            return self.check_for_wall_and_move(new_pos)
        for x_index in range(0, self.position[0]):
            if (new_pos := (x_index, self.position[1])) in self.field:
                return self.check_for_wall_and_move(new_pos)

    def move_left(self):
        if (new_pos := (self.position[0] - 1, self.position[1])) in self.field:
            return self.check_for_wall_and_move(new_pos)
        for x_index in range(self.width + 1, self.position[0], -1):
            if (new_pos := (x_index, self.position[1])) in self.field:
                return self.check_for_wall_and_move(new_pos)

    def move_up(self):
        if (new_pos := (self.position[0], self.position[1] - 1)) in self.field:
            return self.check_for_wall_and_move(new_pos)
        for y_index in range(self.height + 1, self.position[1], -1):
            if (new_pos := (self.position[0], y_index)) in self.field:
                return self.check_for_wall_and_move(new_pos)

    def move_down(self):
        if (new_pos := (self.position[0], self.position[1] + 1)) in self.field:
            return self.check_for_wall_and_move(new_pos)
        for y_index in range(0, self.position[1]):
            if (new_pos := (self.position[0], y_index)) in self.field:
                return self.check_for_wall_and_move(new_pos)

    def make_move(self, direction):
        match direction:
            case 0:
                return self.move_up()
            case 90:
                return self.move_right()
            case 180:
                return self.move_down()
            case 270:
                return self.move_left()

    def check_for_wall_and_move(self, new_pos):
        if self.field[new_pos]:
            self.position = new_pos
            return True
        else:
            return False


def lets_make_big_moves():
    my_map, instructions = read_input()
    my_orientation = 90
    for command in instructions:
        match command:
            case "L":
                my_orientation = (my_orientation - 90) % 360
            case "R":
                my_orientation = (my_orientation + 90) % 360
            case _:
                for i in range(0, command):
                    if not my_map.make_move(my_orientation):
                        break
    end_position = my_map.position
    facing_value = 3
    match my_orientation:
        case 90:
            facing_value = 0
        case 180:
            facing_value = 1
        case 270:
            facing_value = 2
    return (end_position[0] + 1) * 4 + (end_position[1] + 1) * 1000 + facing_value


print(f"That's for Part 1:\nThe final password is: {lets_make_big_moves()}")
# 57350
