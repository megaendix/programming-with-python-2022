def read_input():
    output = []
    all_cords = []
    with open("day18.txt", "rt") as lava:
        for droplet in lava:
            droplet = droplet.strip()
            coordinates = droplet.split(",")
            all_cords.append(int(coordinates[0]))
            all_cords.append(int(coordinates[1]))
            all_cords.append(int(coordinates[2]))
            coordinates = tuple(map(lambda x: int(x), coordinates))
            output.append(coordinates)

    return output, max(all_cords), min(all_cords)


def calculate_neighbours(node, maxi, mini, obsidian):
    x = node[0]
    y = node[1]
    z = node[2]
    neighbours = set()
    neighbours.add((x + 1, y, z))
    neighbours.add((x - 1, y, z))
    neighbours.add((x, y + 1, z))
    neighbours.add((x, y - 1, z))
    neighbours.add((x, y, z + 1))
    neighbours.add((x, y, z - 1))
    to_delete = set()
    for partner in neighbours:
        if partner[0] < mini or maxi < partner[0]:
            to_delete.add(partner)
        if partner[1] < mini or maxi < partner[1]:
            to_delete.add(partner)
        if partner[2] < mini or maxi < partner[2]:
            to_delete.add(partner)
        if partner in obsidian:
            to_delete.add(partner)
    return neighbours - to_delete


def breadth_first_search(obsidian, maxi, mini):
    mini -= 1
    maxi += 1
    surface_count = 0
    current_node = (mini, mini, mini)
    visited = set()
    frontier = []
    while True:
        new_frontier = calculate_neighbours(current_node, maxi, mini, obsidian)

        for node in new_frontier:
            if node in visited:
                continue
            if node in frontier:
                continue
            frontier.append(node)

        visited.add(current_node)

        surface_count += check_neighbours(current_node, obsidian)

        if len(frontier) == 0:
            break

        current_node = frontier.pop(0)

    return surface_count


def count_open_sides():
    drops, maxi, mini = read_input()
    surface_all, obsidian = compare_drops(drops, drops)

    outside_surface = breadth_first_search(obsidian, maxi, mini)

    return surface_all, outside_surface


def check_neighbours(block, obsidian):
    x = block[0]
    y = block[1]
    z = block[2]
    neighbour_count = 0
    neighbours = []
    if (drop := (x, y, z)) in obsidian:
        return None
    if (drop := (x + 1, y, z)) in obsidian:
        neighbours.append(drop)
        neighbour_count += 1
    if (drop := (x - 1, y, z)) in obsidian:
        neighbours.append(drop)
        neighbour_count += 1
    if (drop := (x, y + 1, z)) in obsidian:
        neighbours.append(drop)
        neighbour_count += 1
    if (drop := (x, y - 1, z)) in obsidian:
        neighbours.append(drop)
        neighbour_count += 1
    if (drop := (x, y, z + 1)) in obsidian:
        neighbours.append(drop)
        neighbour_count += 1
    if (drop := (x, y, z - 1)) in obsidian:
        neighbours.append(drop)
        neighbour_count += 1
    return neighbour_count


def compare_drops(drops, drops_to_compare):
    surface_area = 0
    G = dict()
    for drop in drops:
        sides_open = 6
        x = drop[0]
        y = drop[1]
        z = drop[2]
        neighbours = []
        for compare_drop in drops_to_compare:
            if compare_drop == drop:
                continue
            if x == compare_drop[0] and y == compare_drop[1]:
                if z + 1 == compare_drop[2] or z - 1 == compare_drop[2]:
                    neighbours.append(compare_drop)
                    sides_open -= 1
                    continue

            if y == compare_drop[1] and z == compare_drop[2]:
                if x + 1 == compare_drop[0] or x - 1 == compare_drop[0]:
                    neighbours.append(compare_drop)
                    sides_open -= 1
                    continue

            if x == compare_drop[0] and z == compare_drop[2]:
                if y + 1 == compare_drop[1] or y - 1 == compare_drop[1]:
                    neighbours.append(compare_drop)
                    sides_open -= 1
                    continue

            if sides_open == 0:
                break
        if sides_open > 0:
            surface_area += sides_open
            G[drop] = neighbours

    return surface_area, G


every_surface, outside = count_open_sides()

print(f"That's for Part 1:\nMy surface area: {every_surface}")
# 4460
print(f"That's for Part 2:\nMy surface area: {outside}")
# 2498

