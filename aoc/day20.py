def read_input():
    encryption = []
    with open("day20.txt", "rt") as my_file:
        for line in my_file:
            encryption.append(int(line))
    return encryption


class Node:
    def __init__(self, data=None, next=None, turn=0):
        self.data = data
        self.next = next
        self.turn = turn
        self.steps = data


class CircularLinkedList:
    def __init__(self):
        self.head = None
        self.length = 0
        self.tail = None

    def insert_at_beginning(self, data, turn=None):
        if turn is None:
            node = Node(data, self.head, self.length)
        else:
            node = Node(data, self.head, turn)
        self.head = node
        if self.tail is None:
            self.tail = node
            self.tail.next = node
        else:
            self.tail.next = node
        self.length += 1

    def update_node_steps_and_values(self, multiplier=1):
        current_node = self.head
        mod = self.length - 1
        for i in range(0, self.length):
            if current_node.data == 0:
                current_node = current_node.next
                continue
            current_node.data *= multiplier
            if current_node.data < 0:
                current_node.steps = current_node.data % mod
            else:
                if current_node.data >= self.length:
                    current_node.steps = current_node.data % mod
                else:
                    current_node.steps = current_node.data
            current_node = current_node.next

    def get_length(self):
        return self.length

    def mixup(self, turn):
        previous_node = None
        node_to_move = self.head
        for i in range(0, self.length):
            if node_to_move.turn == turn:
                break
            previous_node = node_to_move
            node_to_move = node_to_move.next

        if previous_node is None:
            current_node = self.head
            for i in range(0, self.length):
                if current_node.next == node_to_move:
                    previous_node = current_node
                    break
                current_node = current_node.next

        steps_to_move = node_to_move.steps

        if steps_to_move == 0:
            return
        previous_node.next = node_to_move.next

        for i in range(0, steps_to_move):
            previous_node = previous_node.next

        node_to_move.next = previous_node.next
        previous_node.next = node_to_move

    def iterate_from_zero(self):
        zero_node = self.find_node_with_value(0)
        sum_of_coordinates = 0
        for i in range(0, 3001):
            if i == 1000 or i == 2000 or i == 3000:
                sum_of_coordinates += zero_node.data
            zero_node = zero_node.next
        return sum_of_coordinates

    def find_node_with_value(self, value):
        current_node = self.head
        for i in range(0, self.length):
            if current_node.data == value:
                return current_node
            current_node = current_node.next
        return None

    def print(self):
        if self.head is None:
            print("LinkedList is empty")
            return

        itr = self.head
        llstr = ""

        for i in range(0, self.length):
            llstr += str(itr.turn) + "|" + str(itr.data) + "|" + str(itr.steps) + " ---> "
            itr = itr.next

        llstr += str(itr.turn) + "|" + str(itr.data) + "|" + str(itr.steps) + " ..."
        print(llstr)


def create_llist(multiplier=1):
    file = read_input()
    my_clList = CircularLinkedList()
    file.reverse()
    length = len(file)
    for turn, entry in enumerate(file):
        index = length - 1 - turn
        my_clList.insert_at_beginning(entry * multiplier, index)
    return my_clList


def mix_em_up(multiplier=811_589_153):
    linked_list = create_llist()
    length = linked_list.get_length()
    linked_list.update_node_steps_and_values()
    for turn in range(0, length):
        linked_list.mixup(turn)

    part_one = linked_list.iterate_from_zero()
    print("Done with Part 1")
    print(part_one)

    # ------------------------------
    linked_list = create_llist()
    linked_list.update_node_steps_and_values(multiplier)
    for count in range(0, 10):
        print(f"Start {count} iteration")
        for turn in range(0, length):
            linked_list.mixup(turn)

    part_two = linked_list.iterate_from_zero()

    return part_one, part_two


one, two = mix_em_up()
print(f"That's for Part 1:\nThe sum of the three coordinates is: {one}")
# 3700
print(f"That's for Part 2:\nThe sum of the three coordinates multiplied with 811589153 is: {two}")
# 10626948369382


