def create_config():
    forest_column = []
    forest_map = []
    with open("day08.txt", "rt") as my_forest:
        for line in my_forest:
            column = []
            karte = []
            line = line.strip()
            for height in line:
                column.append(int(height))
                karte.append(True)
            forest_column.append(column)
            forest_map.append(karte)
    return forest_column, forest_map


def check_visibility():
    forest_row_layout, mapping = create_config()
    mapping = check_from_left(forest_row_layout, mapping)
    mapping = check_from_top(forest_row_layout, mapping)
    mapping = check_from_bottom(forest_row_layout, mapping)
    mapping = check_from_right(forest_row_layout, mapping)
    visible_trees = 0
    for r in mapping:
        for c in r:
            if not c:
                visible_trees += 1
    return visible_trees


def check_from_left(layout, mapping):
    index_row = 0
    for row in layout:
        index_column = 0
        max_height = -1
        for height in row:
            if height > max_height:
                max_height = height
                mapping[index_row][index_column] = False
            index_column += 1
        index_row += 1
    return mapping


def check_from_top(forest_row_layout, mapping):
    number_of_rows = len(forest_row_layout)
    number_of_columns = len(forest_row_layout[0])
    for index_column in range(0, number_of_columns):
        max_height = -1
        for index_row in range(0, number_of_rows):
            if forest_row_layout[index_row][index_column] > max_height:
                max_height = forest_row_layout[index_row][index_column]
                mapping[index_row][index_column] = False
    return mapping


def check_from_right(layout, mapping):
    number_of_rows = len(layout)
    number_of_columns = len(layout[0])
    for index_row in range(0, number_of_rows):
        max_height = -1
        for index_column in range(number_of_columns-1, -1, -1):
            if layout[index_row][index_column] > max_height:
                max_height = layout[index_row][index_column]
                mapping[index_row][index_column] = False
    return mapping


def check_from_bottom(layout, mapping):
    number_of_rows = len(layout)
    number_of_columns = len(layout[0])
    for index_column in range(0, number_of_columns):
        max_height = -1
        for index_row in range(number_of_rows-1, -1, -1):
            if layout[index_row][index_column] > max_height:
                max_height = layout[index_row][index_column]
                mapping[index_row][index_column] = False
    return mapping


def scenic_score_to_right(layout, row_index, column_index):
    scenic_score = 0
    number_of_columns = len(layout[0])
    height_of_tree = layout[row_index][column_index]
    for column_index in range(column_index + 1, number_of_columns):
        scenic_score += 1
        if layout[row_index][column_index] >= height_of_tree:
            break
    return scenic_score


def scenic_score_to_bottom(layout, row_index, column_index):
    scenic_score = 0
    number_of_rows = len(layout)
    height_of_tree = layout[row_index][column_index]
    for row_index in range(row_index + 1, number_of_rows):
        scenic_score += 1
        if layout[row_index][column_index] >= height_of_tree:
            break
    return scenic_score


def scenic_score_to_left(layout, row_index, column_index):
    scenic_score = 0
    height_of_tree = layout[row_index][column_index]
    for column_index in range(column_index - 1, -1, -1):
        scenic_score += 1
        if layout[row_index][column_index] >= height_of_tree:
            break
    return scenic_score


def scenic_score_to_upper(layout, row_index, column_index):
    scenic_score = 0
    height_of_tree = layout[row_index][column_index]
    for row_index in range(row_index - 1, -1, -1):
        scenic_score += 1
        if layout[row_index][column_index] >= height_of_tree:
            break
    return scenic_score


def calculate_scenic_score():
    layout, mapping = create_config()
    number_of_rows = len(layout)
    number_of_columns = len(layout[0])
    scenic_score = 0
    for r in range(0, number_of_rows):
        for c in range(0, number_of_columns):
            current_score = scenic_score_to_right(layout, r, c)
            current_score *= scenic_score_to_upper(layout, r, c)
            current_score *= scenic_score_to_left(layout, r, c)
            current_score *= scenic_score_to_bottom(layout, r, c)
            scenic_score = max(scenic_score, current_score)
    return scenic_score


print(f"That's for Part 1:\n{check_visibility()} trees are visible from the outside.\n")
print(f"That's for Part 2:\n{calculate_scenic_score()} is the highest scenic score possible.")
