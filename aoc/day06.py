def buffer_system(f, distinct_chars):
    marker = []
    signal = ""

    with open(f, "rt") as sequence:
        for line in sequence:
            signal = line

    index = 0
    for character in signal:
        if len(marker) != distinct_chars:
            marker.append(character)
            index += 1
            continue

        if len(set(marker)) == distinct_chars:
            return index

        transition_array(marker, character, distinct_chars)
        index += 1


def transition_array(array, element, length):
    assert len(array) == length
    del array[0]
    array.append(element)


index_of_marker = buffer_system("day06.txt", 4)
print(f"That's for Part 1:\nNumber of characters processed: {index_of_marker}\n")

index_of_message = buffer_system("day06.txt", 14)
print(f"That's for Part 2:\nNumber of characters processed: {index_of_message}\n")
