def read_input():
    paired_packets = []
    single_packets = []
    with open("day13.txt", "rt") as my_packets:
        pair = 0
        for packet in my_packets:
            if pair == 2:
                pair = 0
                continue
            if pair == 0:
                paired_packets.append([])
            pair += 1
            evaled_packet = eval(packet)
            paired_packets[-1].append(evaled_packet)
            single_packets.append(evaled_packet)

    return paired_packets, single_packets


def check_paired_packets():
    packets, not_used = read_input()
    sum_of_goodones = 0
    for index, packet in enumerate(packets):
        left = packet[0]
        right = packet[1]
        # print(f"My Index: {index}")
        match iterate_over_values(left, right):
            case 0:
                # print("Not in right order!")
                continue
            case 1:
                sum_of_goodones += (index + 1)
                # print(f"Right Order! New Sum: {sum_of_goodones}")

    return sum_of_goodones


def iterate_over_values(left, right):
    # print(f"The Left: {left}\nThe Right: {right}\n")
    # input()
    if isinstance(left, int) and isinstance(right, int):
        if left > right:
            return 0
        if left < right:
            return 1
        return 2
    if isinstance(left, list) and isinstance(right, list):
        index = 0
        while True:
            if index == len(left) and index == len(right):
                return 2
            if index == len(left):
                return 1
            if index == len(right):
                return 0

            new_left = left[index]
            new_right = right[index]
            result = iterate_over_values(new_left, new_right)
            match result:
                case 0:
                    return 0
                case 1:
                    return 1
                case 2:
                    index += 1
                    continue

    if isinstance(left, int) and isinstance(right, list):
        return iterate_over_values([left], right)

    if isinstance(left, list) and isinstance(right, int):
        return iterate_over_values(left, [right])


def bubble_sort():
    not_used, packets = read_input()
    packets.append([[2]])
    packets.append([[6]])
    num_packets_to_sort = len(packets)
    for count in range(0, num_packets_to_sort):
        for index, packet in enumerate(packets[0:(num_packets_to_sort - 1)]):
            if not iterate_over_values(packet, packets[index + 1]):
                packets[index], packets[index + 1] = swap(packets[index], packets[index + 1])

    # for packet in packets:
    #   print(packet)
    significant_packets = 1
    for index, packet in enumerate(packets):
        if packet == [[2]] or packet == [[6]]:
            significant_packets *= (index + 1)

    return significant_packets


def swap(a, b):
    return b, a


print(f"\nThis is for Part 1:\nThe added indexes of the right ordered ones are: {check_paired_packets()}")
# 5198

print(f"\nThis is for Part 2:\nThe multiplied indexes of the significant packets is: {bubble_sort()}")
# 22344
