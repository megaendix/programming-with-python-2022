def read_input():
    input_file = []
    with open("day25.txt", "rt") as my_file:
        for line in my_file:
            line = line.strip()
            input_file.append(line)
    return input_file


def translate_and_answer():
    input_file = read_input()
    deci_sum = 0
    for snafu in input_file:
        deci_sum += snafu_to_deci_direct(snafu)

    return deci_to_snafu(deci_sum)


def deci_to_fiver(deci):
    isolated_fives = []
    my_divider = 1
    while deci // my_divider > 4:
        my_divider *= 5

    while my_divider >= 1:
        isolated_fives.append(deci // my_divider)
        deci -= isolated_fives[-1] * my_divider
        my_divider = my_divider // 5

    return isolated_fives


def deci_to_snafu(deci):
    fiver = deci_to_fiver(deci)
    snafu = []
    while len(fiver) != 0:
        digit = fiver.pop(-1)
        match digit:
            case 3:
                snafu.append("=")
            case 4:
                snafu.append("-")
            case 5:
                snafu.append("0")
            case _:
                snafu.append(str(digit))
                continue
        if len(fiver) == 0:
            fiver.append(1)
        else:
            fiver[-1] += 1
    snafu.reverse()
    return snafu


def snafu_to_deci_direct(snafu):
    deci = 0
    snafu = snafu[::-1]
    my_multiplicator = 1
    for digit in snafu:
        match digit:
            case "=":
                digit = -2
            case "-":
                digit = -1
        deci += int(digit) * my_multiplicator
        my_multiplicator *= 5
    return deci


my_snafu_list = translate_and_answer()
my_snafu = ""
for char in my_snafu_list:
    my_snafu += char

print(f"That's for Part 1:\nThe input SNAFU number is: {my_snafu}")
# 2-121-=10=200==2==21

