def read_input():
    signal = []
    with open("day10.txt", "rt") as mySignal:
        for instruction in mySignal:
            instruction = instruction.strip()
            match instruction.split():
                case [noop]:
                    signal.append(0)
                case [addx, value]:
                    signal.append(int(value))
    return signal


def draw_pixel(pixel_to_draw, position_of_sprite, crt):
    assert isinstance(crt, str)

    if len(crt.split("\n")[-1]) == 40:
        crt += "\n"

    pixel_to_draw -= 1
    pixel_to_draw = pixel_to_draw % 40

    position_of_sprite = [position_of_sprite - 1, position_of_sprite, position_of_sprite + 1]
    if pixel_to_draw in position_of_sprite:
        crt += "#"
    else:
        crt += "."

    return crt


def calculate_signal_strength():
    signal_strength = 0
    x_register = 1
    cycle = 0
    signal = read_input()
    crt = ""
    for instruction in signal:

        cycle += 1
        # in cycle one now
        crt = draw_pixel(cycle, x_register, crt)
        if (cycle - 20) % 40 == 0:
            signal_strength += cycle * x_register

        if instruction == 0:
            continue

        cycle += 1
        crt = draw_pixel(cycle, x_register, crt)

        if (cycle - 20) % 40 == 0:
            signal_strength += cycle * x_register

        x_register += instruction

    return signal_strength, crt


print(f"That's for Part 1:\nThe sum of signal strengths is: {calculate_signal_strength()[0]}\n\nThat's for Part 2:\n{calculate_signal_strength()[1]}")





