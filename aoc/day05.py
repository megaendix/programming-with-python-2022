import re


def create_stacks(conifguration, num_stacks):
    list_of_stacks = []

    for index in range(0, num_stacks):
        list_of_stacks.append([])

    for row in conifguration:
        stack = 0
        for index in range(0, 4 * num_stacks, 4):
            current_value = row[index + 1]
            if current_value != " ":
                list_of_stacks[stack].append(current_value)
            stack += 1

    return list_of_stacks


def parse_file(f):
    configuration = ""
    num_of_stacks = 0
    moves = []

    with open(f, "rt") as lines:
        configuration_done = False
        for line in lines:

            if not configuration_done:
                configuration += line
                if "1" in line:
                    num_of_stacks = max(map(int, re.findall(r"\d+", line)))
                    configuration_done = True
                continue

            if "move" not in line:
                continue

            move = map(int, re.findall(r"\d+", line))
            moves += move

    # post process configuration
    configuration = configuration.split("\n")
    configuration.reverse()
    del configuration[0:2]

    # post process moves
    move1 = []
    for index in range(0, len(moves), 3):
        move1.append(moves[index:index + 3])
    moves = move1

    stacks = create_stacks(configuration, num_of_stacks)
    return stacks, moves


def be_the_crain(f, crain9001):
    stacks, moves = parse_file(f)

    for move in moves:
        num_blocks = move[0]
        from_stack = move[1] - 1
        to_stack = move[2] - 1

        if crain9001:
            stack_height = len(stacks[from_stack])
            blocks = stacks[from_stack][stack_height - num_blocks:stack_height]
            del stacks[from_stack][stack_height - num_blocks:stack_height]
            stacks[to_stack] += blocks
            continue

        for i in range(0, num_blocks):
            block = stacks[from_stack].pop()
            stacks[to_stack].append(block)

    crates_on_top = ""
    for stack in stacks:
        crate = stack[-1]
        crates_on_top += crate

    return crates_on_top


part1 = be_the_crain("day05.txt", False)
part2 = be_the_crain("day05.txt", True)
print(f"That's for Part 1:\nAfterwards the crates on top are: {part1}\n")
print(f"That's for Part 1:\nAfterwards the crates on top are (with the CrateMover 9001!): {part2}")


