def sortedCaloriesPerElve(f):
    caloryList = [0]
    with open(f, "rt") as elfList:
        i = 0
        for line in elfList:
            if line == "\n":
                i += 1
                caloryList.append(0)
                continue
            if line.endswith("\n"):
                line.replace("\n", "")
            caloryList[i] += int(line)
    return sorted(caloryList, reverse=True)


calorieList = sortedCaloriesPerElve("day01.txt")
maxCalories = max(calorieList)
print(f"That's for Part 1:\nThe Elv with most calories has {max(calorieList)} calories.\n")
sumTop3Calories = sum(calorieList[0:3])
print(f"That's for Part 2:\nThe top 3 Elves have {sumTop3Calories} calories together.")

