def check_ranges():
    containing_pairs = 0
    with open("day04.txt", "rt") as assignments:
        for assignment in assignments:
            my_ranges = splitting_and_stripping(assignment)
            if my_ranges[0] <= my_ranges[2] and my_ranges[1] >= my_ranges[3]:
                containing_pairs += 1
                continue
            if my_ranges[2] <= my_ranges[0] and my_ranges[3] >= my_ranges[1]:
                containing_pairs += 1
                continue
    return containing_pairs


def overlaping_ranges():
    overlaping_ranges = 0
    with open("day04.txt", "rt") as assignments:
        for assignment in assignments:
            my_ranges = splitting_and_stripping(assignment)
            if my_ranges[0] <= my_ranges[2] <= my_ranges[1] or my_ranges[0] <= my_ranges[3] <= my_ranges[1]:
                overlaping_ranges += 1
                continue
            if my_ranges[2] <= my_ranges[0] <= my_ranges[3] or my_ranges[2] <= my_ranges[1] <= my_ranges[3]:
                overlaping_ranges += 1

    return overlaping_ranges


def splitting_and_stripping(assignment):
    assignment = assignment.strip()
    elve1, elve2 = assignment.split(",")
    my_ranges = elve1.split("-")
    my_ranges += elve2.split("-")
    my_ranges = [int(x) for x in my_ranges]
    return my_ranges


pairs = check_ranges()
print(f"That's for Part 1:\nThe count of assignment pairs where one range fully contains the other is: {pairs}\n")
overlapping = overlaping_ranges()
print(f"That's for Part 2:\nThe count of assignment pairs where the ranges overlap is: {overlapping}")
