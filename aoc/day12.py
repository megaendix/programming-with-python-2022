def read_input():
    num_rows = 0
    input_str = ""
    with open("day12.txt", "rt") as the_hill:
        for line in the_hill:
            line.strip()
            num_rows += 1
            input_str += line

    hill_list = list(input_str)
    while "\n" in hill_list:
        hill_list.remove("\n")
    if "\n" in hill_list:
        print("got em")
    num_columns = len(hill_list) // num_rows

    startingIndex = 0
    endIndex = 0
    for index, char in enumerate(hill_list):
        if char == "S":
            startingIndex = index
        if char == "E":
            endIndex = index

    hill_list[startingIndex] = "a"
    hill_list[endIndex] = "z"

    hill_list = list(map(lambda x: ord(x), hill_list))
    G = dict()
    ord_a = ord("a")
    list_of_as = []
    # only store nodes that are reachable from one node
    for index, knot in enumerate(hill_list):
        neighbours = []
        if (not index < num_columns) and knot >= hill_list[index - num_columns] - 1:
            neighbours.append(index - num_columns)
        if (not index % num_columns == 0) and knot >= hill_list[index - 1] - 1:
            neighbours.append(index - 1)
        if (not (index + 1) % num_columns == 0) and knot >= hill_list[index + 1] - 1:
            neighbours.append(index + 1)
        if (not index >= num_columns * (num_rows - 1)) and knot >= hill_list[index + num_columns] - 1:
            neighbours.append(index + num_columns)
        G[index] = neighbours

        if knot == ord_a:
            list_of_as.append(index)

    return G, startingIndex, endIndex, list_of_as


def breadth_first_search(graph, start, end):
    current_node = start
    end_node = end
    visited = set()
    frontier = []
    current_wideness = len(graph[current_node])
    depth = 0
    while True:

        # check if current_node is the goal node
        if current_node == end_node:
            depth += 1
            break

        current_wideness -= 1
        # check if the child of the node are already visited, if not add them too frontier
        for node in graph[current_node]:
            if node in visited:
                continue
            if node in frontier:
                continue
            frontier.append(node)

        # add current node to visited
        visited.add(current_node)

        if current_wideness == 0:
            current_wideness = len(frontier)
            depth += 1

        if len(frontier) == 0:
            return None
        current_node = frontier.pop(0)

    return depth


def find_shortest_path():
    graph, starting_node, end_node, who = read_input()
    depth = breadth_first_search(graph, starting_node, end_node)

    print("Thats for Part 1:")
    print(f"Length of shortest Path: {str(depth)}")


find_shortest_path()
# 468 was the answer


def find_best_hiking_path():
    graph, start_node, end_node, the_As = read_input()
    hiking_list = []
    for new_start in the_As:
        depth = breadth_first_search(graph, new_start, end_node)
        if depth is None:
            continue
        hiking_list.append(depth)

    print("\nThats for Part 2:")
    print(f"Length of shortest Path: {str(min(hiking_list))}")


find_best_hiking_path()
# 459 was the answer