def read_input():
    all_dirs = []
    all_dirs_names = set()
    home = dict()
    all_dirs.append(home)
    home["value"] = 0
    current_dir = home
    with open("day07.txt", "rt") as my_file:
        for line in my_file:
            line = line.replace("$", "")
            line = line.strip()
            match line.split():
                case "cd", "/":
                    current_dir = home
                    continue
                case "ls":
                    continue
                case "cd",  "..":
                    current_dir = current_dir["parent"]
                    continue
                case "cd", directory:
                    current_dir = current_dir[directory]
                    continue

                case "dir", name:
                    new_dict = dict()
                    current_dir[name] = new_dict
                    new_dict["parent"] = current_dir
                    new_dict["value"] = 0
                    all_dirs_names.add(name)
                    continue
                case size, file:
                    size = int(size)
                    current_dir["value"] += size
                    helper = current_dir
                    while "parent" in current_dir:
                        current_dir = current_dir["parent"]
                        current_dir["value"] += size
                    current_dir = helper
                    continue
    return home


def iterate_and_find_values(dictionary, size):
    size_to_add = 0
    for dictio in dictionary:
        match dictio:
            case "parent":
                continue
            case "value":
                if dictionary[dictio] <= size:
                    size_to_add += dictionary[dictio]
            case _:
                size_to_add += iterate_and_find_values(dictionary[dictio], size)
    return size_to_add


def find_dirs_to_delete(dictionary, size):
    dirs_to_delete = []
    for dictio in dictionary:
        match dictio:
            case "parent":
                continue
            case "value":
                if dictionary[dictio] >= size:
                    dirs_to_delete.append(dictionary[dictio])
            case _:
                dirs_to_delete += find_dirs_to_delete(dictionary[dictio], size)
    return dirs_to_delete


def find_small_dirs(size=100_000, disk_space=70_000_000, disk_space_required=30_000_000):
    home = read_input()
    space_filled = home["value"]
    space_free = disk_space - space_filled
    space_needed = disk_space_required - space_free
    return iterate_and_find_values(home, size), min(find_dirs_to_delete(home, space_needed))


print(f"That's for Part 1:\nThe size of all directorys summed up <= 100_000 is: {find_small_dirs()[0]}")
# 1555642
print("")
print(f"That's for Part 2:\nThe size of the directory to delete is: {find_small_dirs()[1]}")
# 5974547