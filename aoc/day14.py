def read_input():
    cave = dict()
    max_x = 0
    max_y = 0
    with open("day14.txt", "rt") as my_cave:
        for rocks in my_cave:
            nodes = rocks.strip().split("->")
            corners = []
            for cord in nodes:
                x = int(cord.split(",")[0])
                y = int(cord.split(",")[1])
                corners.append((x, y))
            for index, corner in enumerate(corners[0:len(corners) - 1]):
                x = corner[0]
                y = corner[1]
                new_x = corners[index + 1][0]
                new_y = corners[index + 1][1]

                max_x = max([max_x, x, new_x])
                max_y = max([max_y, y, new_y])

                if x == new_x:
                    if y > new_y:
                        y, new_y = new_y, y
                    for y_cord in range(y, new_y + 1):
                        cave[x, y_cord] = "#"
                    continue

                if x > new_x:
                    x, new_x = new_x, x
                for x_cord in range(x, new_x + 1):
                    cave[x_cord, y] = "#"
    return cave, max_x, max_y


def print_cave_to_file(cave, max_x, max_y):
    output = ""
    for y in range(0, max_y + 1):
        for x in range(0, max_x + 1):
            if (x, y) in cave:
                output += cave[x, y]
            else:
                output += "."
        output += "\n"
    with open("day14_visual.txt", "w") as f:
        f.write(output)


def let_it_drip(infinite=False):
    cave, max_x, max_y = read_input()
    if infinite:
        cave, max_x, max_y = add_floor(cave, max_y)
    drop_point = (500, 0)
    pos_sand = drop_point
    num_of_chilling_sand = 0
    while check_if_beneath(pos_sand[0], pos_sand[1], cave, max_y):
        x = pos_sand[0]
        y = pos_sand[1]

        if (x, y) in cave:
            break

        if not (x, y + 1) in cave:
            pos_sand = (x, y + 1)
            continue

        if not (x - 1, y + 1) in cave:
            pos_sand = (x - 1, y + 1)
            continue

        if not (x + 1, y + 1) in cave:
            pos_sand = (x + 1, y + 1)
            continue

        cave[pos_sand] = "o"
        num_of_chilling_sand += 1
        pos_sand = drop_point

    print_cave_to_file(cave, max_x, max_y)
    return num_of_chilling_sand


def check_if_beneath(x, y, cave, max_y):
    for new_y in range(y + 1, max_y + 1):
        if (x, new_y) in cave:
            return True
    return False


def add_floor(cave, max_y):
    max_y += 2
    y_cord_floor = max_y
    left_x = 500 - max_y
    right_x = 500 + max_y

    for x_cord in range(left_x, right_x + 1):
        cave[x_cord, y_cord_floor] = "#"

    # print_cave_to_file(cave, right_x, max_y + 2)
    return cave, right_x, max_y



print(f"That's for Part 1:\nUnits of Sand at rest: {let_it_drip()}")
# 961

print(f"That's for Part 2:\nUnits of Sand at rest: {let_it_drip(True)}")
# 26375



