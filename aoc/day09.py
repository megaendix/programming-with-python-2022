class Point:
    def __init__(self, x=0, y=0):
        self.x, self.y = x, y

    def manhattan(self):
        return sum(map(abs, (self.x, self.y)))

    def check_if_touching(self, other):
        return self.check_if_one_apart(other) or self.check_if_diagonally_apart(other) or \
            self == other

    def check_if_one_apart(self, other):
        return abs(self.x - other.x) + abs(self.y - other.y) == 1

    def check_if_diagonally_apart(self, other):
        return abs(self.x - other.x) == 1 and abs(self.y - other.y) == 1

    def moveUp(self):
        return Point(self.x, self.y + 1)

    def moveDown(self):
        return Point(self.x, self.y - 1)

    def moveLeft(self):
        return Point(self.x - 1, self.y)

    def moveRight(self):
        return Point(self.x + 1, self.y)

    def tupple_rep(self):
        return self.x, self.y

    def on_same_line(self, other):
        return (self.x == other.x and self.y != other.y) or (self.x != other.x and self.y == other.y)

    def move_diagonally(self, other):
        match self.relative_quadrant(other):
            case 0:
                return Point(self.x + 1, self.y + 1)
            case 1:
                return Point(self.x + 1, self.y - 1)
            case 2:
                return Point(self.x - 1, self.y - 1)
            case 3:
                return Point(self.x - 1, self.y + 1)

    def move_in_line(self, other):
        if self.x < other.x:
            return self.moveRight()
        if self.x > other.x:
            return self.moveLeft()
        if self.y < other.y:
            return self.moveUp()
        return self.moveDown()


    def relative_quadrant(self, other):
        if self.x < other.x:
            if self.y < other.y:
                return 0
            return 1
        if self.y < other.y:
            return 3
        return 2

    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Point(self.x - other.x, self.y - other.y)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __repr__(self):
        return f"Point({self.x}, {self.y})"

    def move(self, direction):
        match direction:
            case "U":
                return self.moveUp()
            case "D":
                return self.moveDown()
            case "L":
                return self.moveLeft()
            case "R":
                return self.moveRight()


def parse_file():
    moves = []
    with open("day09.txt", "rt") as my_movements:
        for move in my_movements:
            move = move.strip()
            direction, times = move.split()
            moves.append((direction, int(times)))
    return moves


def move_head(head, direction):
    if not isinstance(head, Point):
        raise Exception
    match direction:
        case "U":
            return head.moveUp()
        case "D":
            return head.moveDown()
        case "L":
            return head.moveLeft()
        case "R":
            return head.moveRight()


def play_ropegame_extended(number_of_knots=2):
    moves = parse_file()
    snake = []
    for i in range(0, number_of_knots):
        snake.append(Point())
    points_visited = {snake[-1].tupple_rep()}

    for move in moves:
        times = move[1]
        direction = move[0]
        for n in range(0, times):
            snake[0] = snake[0].move(direction)
            for index in range(1, len(snake)):
                if snake[index].check_if_touching(snake[index - 1]):
                    continue
                if snake[index].on_same_line(snake[index - 1]):
                    snake[index] = snake[index].move_in_line(snake[index-1])
                else:
                    snake[index] = snake[index].move_diagonally(snake[index - 1])
            points_visited.add(snake[-1].tupple_rep())

    return len(points_visited)


print(f"That's for Part 1:\nThe Tail with 2 nodes visits {play_ropegame_extended(2)} different positions.\n")
print(f"That's for Part 2:\nThe Tail with 10 nodes visits {play_ropegame_extended(10)} different positions.")
